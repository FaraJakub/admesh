![](doc/Images/ADmesh_logo_text_mini.png )

## Instalation

For instalation:

1. clone the repository: `git clone https://FaraJakub@bitbucket.org/FaraJakub/admesh.git`
2. go to repository: `cd admesh`
3. install admesh: `make`  
In one step:`git clone https://FaraJakub@bitbucket.org/FaraJakub/admesh.git && cd admesh && make`  
---
## About
This code is an extention to FEniCS project https://fenicsproject.org . This code allowes to non-uniformly adapt mesh in 2D. The main purpoes is to adapt mesh in ALE (Arbitrary Lagrangian Eulerian), or another time deppending interaction method. This code makes the computations in serial, however it can be run as a part of parallel code and the mesh will be redistributed to each core.

---
## Examples

The example of this code can be found in admesh/admesh4py/examples.

---
##