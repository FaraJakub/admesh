import admesh4py.admesh4py as admesh
from dolfin import *
comm = MPI.comm_world
rank = comm.Get_rank()
remesh_params = {"num_iter":5,
                 "Marge_cells":True,
	         "Add_Vertex_On_Edge":True,
	         "Flip_Edges":True,
                 "Remove_Bndry_Edge":True,
	         "Remove_Vertices":True,
                 "Move_Vertices":False,
	         "Report":True}

if __name__=="__main__":
    resolution = 40
    mesh = UnitSquareMesh(resolution,resolution)
    Q = VectorFunctionSpace(mesh,"CG",2)
    u = Function(Q)
    exp = Expression(('0.1*x[1]','0.1*x[0]'),degree = 1)
    u = project(exp,Q)
    mesh_file = 'mesh.xdmf'
    xdmf = XDMFFile(mesh.mpi_comm(), mesh_file)
    xdmf.write(mesh)
    
    def bndry_func(x,y):
        return x*(x-1.)*y*(y-1.)
    
    m = admesh.admesh(mesh_file,comm)
    m.set_remesh_params(remesh_params)

    #m.set_bndry(bndry_func)
    
    V = FunctionSpace(m.mesh,"CG",1)
    params_expr = Expression('0.5 +4.0*(pow(x[1]-0.5,4) + pow(x[0]-0.5,2))',degree = 2)
    #params_expr = Expression('1.0 + 0.2*sin(3.14*x[1])',degree = 2)
    #params_expr = Expression('0.8 + 0.9*x[1]',degree = 2)

 
    m.set_remesh_function(params_expr)
    #m.move(u)
    r_min = m.rmin()
    r_max = m.rmax()

    m.remesh(r_min*0.7,r_max*1.5)
    #mesh = m.get_mesh()

    #m.make_movie(r_min*0.7,r_max*1.7)
    mesh = m.get_mesh()
    m.plot()
    
