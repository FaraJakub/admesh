import admesh4py.admesh4py as admesh
from dolfin import *
from mshr import *
import matplotlib.pyplot as plt
import numpy as np
#from fenicstools.Interpolation import interpolate_nonmatching_mesh,interpolate_nonmatching_mesh_any

comm = MPI.comm_world
rank = comm.Get_rank()
parameters["ghost_mode"] = "none"

#__________Parameters__________
P1 = Point(0.0,0.0)
P2 = Point(2.0,1.0)
S = Point(0.5,0.5)
r = 0.2
resolution = 40

OUTER = 1
INNER = 2

num_of_points = 60 #num of points on surface
mesh_file = 'mesh.xdmf'
cell_function_file = "cell_function.xml"
remesh_params = {"num_iter":4,
                 "Marge_cells":True,
	         "Add_Vertex_On_Edge":True,
	         "Flip_Edges":True,
                 "Remove_Bndry_Edge":True,
	         "Remove_Vertices":False,
                 "Move_Vertices":False,
	         "Report":False}

#______________________________

def make_points_circle(S,r,max_points):
    alpha = 0.0
    points  = [Point(S[0]+r,S[1])]
    for i in range(max_points-1):
        alpha += np.pi*2/max_points
        points.insert(len(points),Point(S[0]+r*cos(alpha),S[1]+r*sin(alpha)))
    return (points)


def make_mesh(P1,P2,S,r,num_of_points):
    points = make_points_circle(S,r,num_of_points)
    Omega = Rectangle(P1,P2)
    Omega_i = Polygon(points)
    Omega.set_subdomain(OUTER,Omega-Omega_i)
    Omega.set_subdomain(INNER,Omega_i)
    mesh = generate_mesh(Omega,resolution)

    #self mesh
    xdmf = XDMFFile(mesh.mpi_comm(), mesh_file)
    xdmf.write(mesh)
    
    marker = MeshFunction("size_t", mesh, 2, mesh.domains())
    #save cell function
    File(cell_function_file)<<marker

if __name__=="__main__":
    #_______________Build_mesh____________________
    #___this part has NOT to be run in parallel___
    #make_mesh(P1,P2,S,r,num_of_points)
    #______________________________________________
    #_______________Build_admesh___________________
    m = admesh.admesh(mesh_file,comm)

    m.set_remesh_params(remesh_params)
    m.set_marks(cell_function_file,INNER)
    #_______________________________________________
    
    def boundary(x, on_boundary):
        tol = 1E-14
        return on_boundary

    def bndry_func(x,y):
        return ((x-S[0])**2 + (y-S[1])**2-r**2) 
    m.set_bndry(bndry_func)
    N = 80
    u = Expression(('0.1*x[0]*(x[0]-2.0)*x[1]*(x[1]-1.0)','0.0 '),degree = 4)
    r_min = m.rmin()
    r_max = m.rmax()
    m.remesh((r_min+r_max)*0.3,(r_min+r_max)*0.6)
    mesh = m.get_mesh()
    V = VectorFunctionSpace(mesh,"CG",1)
    v_expr = Expression(('exp(-( pow(0.5-x[0],2) + pow(0.5-x[1],2) ))','x[0]'),degree = 8)
    v = project(v_expr,V)
   
    directory = 'result'
    
    with XDMFFile(comm,"{}/{}.xdmf".format(directory,'v') ) as f:
        f.parameters["flush_output"] = True
        f.parameters["functions_share_mesh"] = True
    
    with XDMFFile(comm,"{}/{}.xdmf".format(directory,'u') ) as f_u:
        f_u.parameters["flush_output"] = True
        f_u.parameters["functions_share_mesh"] = True


    f.write(v,0)
    for i in range(1,N):
        
        U = VectorFunctionSpace(mesh,"CG",1)
        bc = DirichletBC(U, Constant((0.0,0.0)), boundary)
        u_p = project(u,U)
        bc.apply(u_p.vector())
        u_p.rename('u','u')
        f_u.write(u_p,i)

        v_val = v.vector().get_local()
        m.move(u_p)

        V = VectorFunctionSpace(mesh,"CG",1)
        
        v_new = Function(V)
        v_new.vector().set_local(v_val)
        v_new.vector().apply('insert')

 
        m.remesh((r_min+r_max)*0.3,(r_min+r_max)*0.6)
        mesh = m.get_mesh()
        V = VectorFunctionSpace(mesh,"CG",1)        
        mesh_function = m.get_mesh_function()
        v = m.project(v_new,V)
        
        v.rename('v','v')
        f.write(v,i)
        if rank ==0:
            print(f"step = {i}/{N}",flush = True,end = "\r")
        
