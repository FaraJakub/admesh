"""Main module."""

import admesh as msr
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
import os

parameters["ghost_mode"] = "shared_facet"
#parameters.mesh_partitioner = 'ParMETIS'
code="""
#include <dolfin/mesh/MeshPartitioning.h>
#include <pybind11/pybind11.h>

namespace dolfin {
    void build_distributed_mesh(std::shared_ptr<Mesh> mesh)
    {
        MeshPartitioning::build_distributed_mesh(*mesh);
    }

};
PYBIND11_MODULE(SIGNATURE, m)
{
    m.def("build_distributed_mesh", &dolfin::build_distributed_mesh);
}
"""

class admesh(object):
    def __init__(self,file_mesh,comm):
        self.rank = comm.Get_rank()
        self.comm = comm
        if self.rank ==0:
            color = 0
        else:
            color = 1
        self.local_comm = comm.Split(color)
        self.local_rank = self.local_comm.Get_rank()
        self.load_mesh_from_file(file_mesh,self.local_comm)
        self.cells = self.mesh.cells()
        self.coor = self.mesh.coordinates()
        self.edges = self.Edges()
        self.V = VectorFunctionSpace(self.mesh,"CG",2)
        self.v = Function(self.V)
        self.U = FunctionSpace(self.mesh,"CG",1)        
   
        self.mod = compile_cpp_code(code)
        self.mesh_global = Mesh(self.mesh)
        self.mod.build_distributed_mesh(self.mesh_global)

        if self.rank==0:
            self.MSR = msr.Mesher(self.coor,self.cells,self.edges)
            self.v2d=vertex_to_dof_map(self.U)
            self.d2v=dof_to_vertex_map(self.U)
        self.remesh_params = {"num_iter":4,
                              "Marge_cells":False,
	                      "Add_Vertex_On_Edge":True,
	                      "Flip_Edges":True,
                              "Remove_Bndry_Edge":True,
	                      "Remove_Vertices":True,
                              "Move_Vertices":True,
	                      "Report":False}
        
    def load_mesh_from_file(self,file_mesh,comm):
        file_extension = os.path.splitext(file_mesh)[1]
        self.mesh = Mesh(comm)
        
        if file_extension == '.h5':
            with HDF5File(comm,file_mesh,'r') as f:
                f.read(self.mesh,'/mesh',False)

        elif file_extension == '.xdmf': 
            with XDMFFile(comm,file_mesh) as xdmf:
                xdmf.read(self.mesh)
        else:
            raise Exception(f'The file extention {file_extention} can not be used for mesh file.')
        
    def load_cell_function(self,file_function,comm,name = '/subdomain'):
        file_extension = os.path.splitext(file_function)[1]
        
        if file_extension == '.h5':
            cell_function = MeshFunction('size_t', self.mesh, self.mesh.topology().dim(), 0)
            with HDF5File(self.mesh.mpi_comm(),file_function,'r') as f:
                f.read(cell_function,name)
            return cell_function

        elif file_extension == '.xml':
            return MeshFunction('size_t', self.mesh, file_function)

        else:
            raise Exception(f'The file extention {file_extention} can not be used for cell function file.')

        
    def Edges(self):
        for e in edges(self.mesh):
            pass
        Edges = np.zeros((e.index()+1,2),dtype = 'int32')
        for e in edges(self.mesh):
            Edges[e.index()] = e.entities(0)
        return Edges

    def rmin(self):
        if self.rank ==0:
            return self.MSR.get_rmin()
        else: return 0
    
    def rmax(self):
        if self.rank ==0:
            return self.MSR.get_rmax()
        else: return 0


    def set_bndry(self,func,eps = 1e-5):
        if self.rank ==0:
            j = 0
            boundary_vertices = np.zeros(self.coor.shape[0],dtype = 'int32')
            for i in range(self.coor.shape[0]): 
                if abs(func(self.coor[i,0],self.coor[i,1]))<eps:
                    boundary_vertices[j] = i
                    j+=1
            boundary_vertices = boundary_vertices[0:j]
            self.MSR.set_boundary(boundary_vertices)
            
    def set_marks(self,mesh_function_file,val):
        marks = []
        if self.rank ==0:
            #mesh_function = MeshFunction('size_t', self.mesh, mesh_function_file)
            mesh_function = self.load_cell_function(mesh_function_file,self.mesh.mpi_comm())
            k = 0
            for i,c in enumerate(cells(self.mesh)):
                if mesh_function[c] == val:
                    marks.append(i)
                    k+=1
            marks_array = np.array(marks,dtype = 'int32')
            self.MSR.set_marks(marks_array,val)
            
    def get_mesh_function(self):
        mesh_function =  MeshFunction("size_t", self.mesh_global, self.mesh_global.topology().dim(), 0)

        if self.rank == 0:
            marks = self.MSR.getMarks()
        else:
            marks = np.array([])
        marks = self.comm.bcast(marks)

        for c in cells(self.mesh_global,'all'):
            mark = marks[c.global_index()]
            mesh_function[c] = mark
        return mesh_function
    
    def move(self,u):
        ALE.move(self.mesh_global,u)
        coor_local = self.mesh_global.coordinates()
        length = np.array([0])
        if self.rank == 0:
            length[0] = self.MSR.num_of_vertices()
        self.comm.Bcast(length,root = 0)
        length = length[0]
        coor = np.zeros((length,2))
        coor.fill(-1000)
        coor[self.mesh_global.topology().global_indices(0)] = coor_local
        l = self.comm.gather(coor)

        if self.rank == 0:
            for l_ in l:
                coor = np.maximum(l_,coor)
            self.MSR.change_coordinates(coor)

 
    def remesh(self,hmin,hmax):
        if self.rank==0:
            self.MSR.remesh(hmin,hmax,
                            num_iter = self.remesh_params["num_iter"],
                            Marge_cells = self.remesh_params["Marge_cells"],
	                    Add_Vertex_On_Edge = self.remesh_params["Add_Vertex_On_Edge"],
	                    Flip_Edges = self.remesh_params["Flip_Edges"],
                            Remove_Bndry_Edge = self.remesh_params["Remove_Bndry_Edge"],
	                    Remove_Vertices = self.remesh_params["Remove_Vertices"],
                            Move_Vertices = self.remesh_params["Move_Vertices"],
	                    Report = self.remesh_params["Report"])
            self.coor = self.MSR.getCoordinates()
            self.cells = self.MSR.getCells()
            
    def set_remesh_params(self,new_params):
        for name,p in new_params.items():
            self.remesh_params[name] = p
            
    def set_remesh_function(self,f,proj = None):
        if proj ==None:
            f_proj = project(f,self.U)
        else:
            f_proj = proj(f,self.comm,self.U)
        #try:
        #    f_proj = proj(f,self.U)
        #except ValueError:
        #    print('Something wrong with function!!')
        if self.rank ==0:
            f_vec = f_proj.vector()[:]
            f_vec = f_vec[self.v2d]
            self.MSR.set_remesh_params(f_vec)
            
    def plot(self):
        import matplotlib.pyplot as plt
        if self.rank ==0:
            plot(self.mesh)
            plt.show()
            
    def make_movie(self,hmin,hmax,mesh_file = "result_mesher"):
        i = 0
        xdmf = XDMFFile(self.mesh.mpi_comm(), mesh_file + "/mesh" + ".xdmf")
        operations_ = ["add_vertex_on_edge","flip_edges","remove_vertex"]
        operations = []
        for _ in range(self.remesh_params["num_iter"]):
            operations.append(operations_)
        for  operation in operations:
            k = 0
            while k!= -1:
                self.coor = self.MSR.getCoordinates()
                self.cells = self.MSR.getCells()
                mesh = self.get_mesh()
                U = FunctionSpace(mesh,"CG",1)
                u = Function(U)
                xdmf.write(u,i)
                k = self.MSR.make_one_step(hmin,hmax,k,operation)
                i+=1
                print(f"step : {i}",flush = True,end = "\r")
                
    def project(self,v,V):
        if self.comm.size ==1:
            u = project(v,V)
        else:
            u = Function(V)
            v.set_allow_extrapolation(True)
            A = PETScDMCollection.create_transfer_matrix\
                (v.ufl_function_space(),
                 u.ufl_function_space())
            u.vector()[:] = A*v.vector()
        return u

    def set_inner_bndry(self):
        if self.rank == 0:
            self.MSR.set_inner_bndry()
                
            
    def get_mesh(self):
        #m = Mesh()
        self.mesh_global = Mesh(self.comm)
        #self.mesh = Mesh(self.local_comm)
        #self.mesh_global = UnitSquareMesh(4,4)
        #print(self.rank)
        if self.rank == 0:
            editor = MeshEditor()
            n_coords = self.coor.shape[0] 
            n_cls = self.cells.shape[0]
                        
            editor.open(self.mesh_global,'triangle', 2, 2)  # top. and geom. dimension are both 2
            editor.init_vertices(n_coords)  # number of vertices
            editor.init_cells(n_cls)     # number of cells
            for i in range(n_coords):
                editor.add_vertex(i,self.coor[i])
            for i in range(n_cls):
                if self.cells[i,0]>= 0 and self.cells[i,1] >=0 and self.cells[i,2]>=0:
                    editor.add_cell(i,self.cells[i])
                else:
                    print("wrong cell",self.cells[i])
            editor.close()

            
        else:
            editor = MeshEditor()
            editor.open(self.mesh_global,'triangle', 2, 2)  # top. and geom. dimension are both 2
            editor.init_vertices(3)  # number of vertices
            editor.init_cells(1)     # number of cells
            editor.add_vertex(0,[0,0])
            editor.add_vertex(1,[1,0])
            editor.add_vertex(2,[0,1])
            
            editor.add_cell(0,[0,1,2])
            editor.close()
            
    

        self.mesh = Mesh(self.mesh_global)
        self.mod.build_distributed_mesh(self.mesh_global)
        self.V = VectorFunctionSpace(self.mesh,"CG",1)
        self.v = Function(self.V)
        
        self.U = FunctionSpace(self.mesh,"CG",1)
        self.u = Function(self.U)

        if self.rank==0:
           self.v2d=vertex_to_dof_map(self.U)
           self.d2v=dof_to_vertex_map(self.U)

        return self.mesh_global
