======
ADmesh
======


.. image:: https://img.shields.io/pypi/v/admesh.svg
        :target: https://pypi.python.org/pypi/admesh

.. image:: https://img.shields.io/travis/audreyr/admesh.svg
        :target: https://travis-ci.com/audreyr/admesh

.. image:: https://readthedocs.org/projects/admesh/badge/?version=latest
        :target: https://admesh.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Adaptiv Dynamic Mesher


* Free software: MIT license
* Documentation: https://admesh.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
