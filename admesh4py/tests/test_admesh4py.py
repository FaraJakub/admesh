#!/usr/bin/env python

"""Tests for `admesh` package."""


import unittest
#from click.testing import CliRunner

from admesh4py.admesh4py import admesh 
from admesh4py import cli
from dolfin import *


class TestAdmesh4py(unittest.TestCase):
    """Tests for `admesh` package."""
    def __init__(self):
        pass

    def test_get_mesh(self):
        comm = MPI.comm_world
        rank = comm.Get_rank()
        resolution = 50
        self.mesh = UnitSquareMesh(resolution,resolution)
        mesh_file = 'mesh.xdmf'
        xdmf = XDMFFile(self.mesh.mpi_comm(), mesh_file)
        xdmf.write(self.mesh)
        m = admesh(mesh_file,comm)
        m.plot()
        new_mesh = m.get_mesh()
        #new_cells = new_mesh.cells()
        print(new_mesh)
        assert self.mesh.cells()[:] == new_mesh.cells()[:]

    # def test_command_line_interface(self):
    #     """Test the CLI."""
    #     runner = CliRunner()
    #     result = runner.invoke(cli.main)
    #     assert result.exit_code == 0
    #     assert 'admesh.cli.main' in result.output
    #     help_result = runner.invoke(cli.main, ['--help'])
    #     assert help_result.exit_code == 0
    #     assert '--help  Show this message and exit.' in help_result.output

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from dolfin import *
    comm = MPI.comm_world
    rank = comm.Get_rank()
    resolution = 50
    mesh = UnitSquareMesh(resolution,resolution)
    Q = VectorFunctionSpace(mesh,"CG",2)
    u = Function(Q)
    exp = Expression(('0.1*x[1]','0.1*x[0]'),degree = 1)
    u = project(exp,Q)
    mesh_file = 'mesh.xdmf'
    xdmf = XDMFFile(mesh.mpi_comm(), mesh_file)
    xdmf.write(mesh)
    
    def bndry_func(x,y):
        return x*(x-1.)*y*(y-1.)
    
    m = admesh(mesh_file,comm)
    m.set_bndry(bndry_func)
    
    V = FunctionSpace(m.mesh,"CG",1)
    #vertex_to_dof_map = V.dofmap().vertex_to_dof_map(mesh)
    #dof_to_vertex_map = dof_to_vertex_map(V)
    v2d=vertex_to_dof_map(V)
    d2v=dof_to_vertex_map(V)
    #v2d = v2d.reshape((-1, m.mesh.geometry().dim()))
    #d2v = d2v[xrange(0, len(d2v), 2)]/2
    params = Function(V)
    #params_expr = Expression('0.01 + 0.1*x[1]',degree = 2)
    params_expr = Expression('0.01+0.1*(pow(x[1]-0.5,2)+pow(x[0]-0.5,2))',degree = 2)
 
    m.set_remesh_params(params_expr)
    #m.move(u)
    m.remesh(0.2,0.5)
    
    mesh = m.get_mesh()
    comm.Barrier()
    m.plot()
    plot(m.mod.build_distributed_mesh(m.mesh))
    plt.show()
    #T = TestAdmesh4py()
    #T.test_get_mesh()
