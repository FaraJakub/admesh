#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <iostream>
#include <vector>
#include "mesher.h"

namespace py = pybind11;
using namespace std;

PYBIND11_MODULE(admesh, m) {
    py::class_<Mesher>(m, "Mesher")
      .def(py::init<py::array_t<double>,py::array_t<int>,py::array_t<int> &>())
      .def("getCells", &Mesher::getCells)
      .def("num_of_vertices", &Mesher::num_of_vertices)
      .def("getMarks",&Mesher::getMarks)
      .def("getBoundaryCoordinates",&Mesher::getBoundaryCoordinates)
      .def("get_rmin",&Mesher::get_rmin)
      .def("get_rmax",&Mesher::get_rmax)
      .def("remove_vertex",&Mesher::remove_vertex_i)
      .def("add_vert_edge",&Mesher::add_vert_edge_i)
      .def("flip_edge",&Mesher::flip_edge_i)
      .def("marge_cells",&Mesher::marge_cells_i)
      .def("refine_cell",&Mesher::refine_cell)
      .def("move_mesh",&Mesher::move_mesh)
      .def("move_vertices",&Mesher::move_vertices)
      .def("set_boundary",&Mesher::set_boundary)
      .def("set_inner_bndry",&Mesher::set_inner_bndry)
      .def("set_marks",&Mesher::set_marks)
      .def("set_remesh_params",&Mesher::set_remesh_params)    
      .def("repair_mesh", &Mesher::repair_mesh)
      .def("change_coordinates",&Mesher::change_coordinates)
      .def("make_one_step",&Mesher::make_one_step)
      .def("remesh",&Mesher::remesh,
	   py::arg("r_min"),
	   py::arg("r_max"),
	   py::arg("num_iter") = 5,
	   py::arg("Marge_cells") = true,
	   py::arg("Add_Vertex_On_Edge") = true,
	   py::arg("Flip_Edges") = true,
	   py::arg("Remove_Bndry_Edge") = true,
	   py::arg("Remove_Vertices") = true,
	   py::arg("Move_Vertices") = true,
	   py::arg("Report") = true)
      .def("getCoordinates", &Mesher::getCoordinates);
}
