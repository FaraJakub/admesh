#include "mesher.h"

void Mesher::set_remesh_params(const py::array_t<double> params){
  py::buffer_info Params_buffer = params.request();
  if (Params_buffer.ndim != 1)
    throw std::runtime_error("Number of dimensions must be 1!");
  double* params_array = (double*) Params_buffer.ptr;
  long unsigned int Y = Params_buffer.shape[0];
  if (Y != Vertices.size()){
    throw std::runtime_error("Number parameters is not equal to number of vertices");
  }
  
  int i = 0;
  for (auto &v:Vertices){
    v->remesh_param =params_array[i];
    i+=1;
  }
}

void Mesher::set_boundary(py::array_t<int> Indices){
  py::buffer_info Indices_buffer = Indices.request();
  vertex *v; 
  //cout<<"here c++"<<endl;
  if (Indices_buffer.ndim != 1)
    throw std::runtime_error("Number of dimensions must be 1!");
  int* ind_array = (int*) Indices_buffer.ptr;
  int Y = Indices_buffer.shape[0];
  for (int ind = 0;ind<Y;ind+=1){
    //cout<<ind_array[ind]<<" \n";
    v = Vertices[ind_array[ind]];
    v->bndry = true;
    for(auto &e : v->Edges){
      if (e->vert1->bndry ==true && e->vert2->bndry ==true){
	e->bndry = true;
      }
    }
  }
  //cout<<"end \n";
}

void Mesher::set_inner_bndry(){
  for (auto &e: Edges){
    if (e->Cells.size() == 2){
      if (e->Cells[0]->mark != e->Cells[1]->mark){
	e->interface = true;
	e->vert1->interface = true;
	e->vert2->interface = true;
      }
    }
  }
}

void Mesher::set_marks(const py::array_t<int> cls,const int value){
  py::buffer_info Indices_buffer = cls.request();
  if (Indices_buffer.ndim != 1)
    throw std::runtime_error("Number of dimensions must be 1!");
  int* ind_array = (int*) Indices_buffer.ptr;
  int Y = Indices_buffer.shape[0];
  for (int ind = 0;ind<Y;ind+=1){
    Cells[ind_array[ind]]->mark = value;
  }
}

void Mesher::change_coordinates(const py::array_t<double> new_coor){
  py::buffer_info Coor_buffer = new_coor.request();
  //if (Coor_buffer.ndim != 1)
  //  throw std::runtime_error("Number of dimensions must be 1!");
  double* coor = (double*) Coor_buffer.ptr;
  int Y = Coor_buffer.shape[0];
  for (int ind = 0;ind<Y;ind+=1){
    Vertices[ind]->x = coor[2*ind];
    Vertices[ind]->y = coor[2*ind+1];
  }  
}

void Mesher::set_outer_bndry(){
  for (auto &e:Edges){
    if (e->Cells.size()==1){
      e->vert1->bndry = true;
      e->vert2->bndry = true;
      e->Cells[0]->bndry = true;
      e->bndry = true;
    }
  }
}
