#include "mesher.h"


void Mesher::marge_cells_i(int index){
  edge *e = Edges[index];
  if (e->usable ==true){
    marge_cells(e);
  }else{cout<<"Warning you tried to marge cells of unusable edge"<<endl;}
}

void Mesher::marge_cells(edge *e){
  cell *c1 = e->Cells[0];
  cell *c2 = e->Cells[1];
  vertex *v1 = e->vert1;
  vertex *v2 = e->vert2;

  double max1 = 0.0;
  double max2 = 0.0;
  double len;
  for (auto &e_ : v1->Edges){
    len = edge_len(e_);
    if (len>max1){max1 = len;}
  }
  for (auto &e_ : v2->Edges){
    len = edge_len(e_);
    if (len>max2){max2 = len;}
  }
  if (max1>max2){
    v2 = e->vert1;
    v1 = e->vert2;
  }
  if (is_starshaped_polygon(v1,v2) == false){
    return;
  }
  
  c1->d = -1;
  c2->d = -1;
  e->usable = false;

  //vertex *v3 = e->vert1;
  //vertex *v4 = e->vert2;

  v1->index = -1;
  edge *e1,*e2,*e3,*e4;
  cell *C1,*C2;
  for (auto &e_: c1->Edges){
    if (e_->usable == true){
      if (e_->vert1 == v1 || e_->vert2 == v1){
	e1 = e_;
	e1->usable = false;
	for (auto &c: e1->Cells){
	  if (c->d !=-1){
	    C1 = c;
	  }
	}
      }else{
	e2 = e_;
      }
    }
  }
  for (auto &e_: c2->Edges){
    if (e_->usable == true){
      if (e_->vert1 == v1 || e_->vert2 == v1){
	e4 = e_;
	e4-> usable = false;
	for (auto &c: e4->Cells){
	  if (c->d !=-1){
	    C2 = c;
	  }
	}
      }else{
	e3 = e_;
      }
    }
  }
  e3->Cells.push_back(C2);
  e2->Cells.push_back(C1);
  
  for (auto c = v1->Cells.begin(); c != v1->Cells.end(); c++) {
    if ((*c)->d ==-1){
      v1->Cells.erase(c--);
    }else{
      for (auto &v:(*c)->Vertices){
	if (v==v1){
	  v = v2;
	  break;
	}
      }
    }
  }
  for (auto &c : v1->Cells){
    for (auto &e_ : c->Edges){
      if (e_->vert1->index ==-1){e_->vert1 = v2;}
      else if (e_->vert2->index ==-1){e_->vert2 = v2;}
    }
  }
  for (auto c = v2->Cells.begin(); c != v2->Cells.end(); c++) {
    if ((*c)->d ==-1){
      v2->Cells.erase(c--);
    }
  }
  vector<vertex*> vertices = {v1,v2};
  vector<cell*> cells = {c1,c2};
  vector<edge*> edges = {e2,e3};

  for(auto e_ =  C1->Edges.begin();e_ !=C1->Edges.end();e_++ ){
    if( (*e_)->usable==false){
      C1->Edges.erase(e_--);
      break;
    }
  }
  for(auto e_ =  C2->Edges.begin();e_ !=C2->Edges.end();e_++ ){
    if( (*e_)->usable==false){
      C2->Edges.erase(e_--);
      break;
    }
  }
  C1->Edges.push_back(e2);
  C2->Edges.push_back(e3);

  for (auto &c: cells){
    for (auto &v:c->Vertices){
      if ( v != v1 && v!=v2){
	vertices.push_back(v);
      }
    }
  }

  for (auto &v: vertices){
    for (auto e_ = v->Edges.begin();e_ != v->Edges.end();e_++){
      if((*e_)->usable ==false){
	v->Edges.erase(e_--);
      }
    }
    for (auto c_ = v->Cells.begin();c_ != v->Cells.end();c_++){
      if((*c_)->d ==-1){
	v->Cells.erase(c_--);
      }
    }
  }
  for (auto &c:v1->Cells){
    if(c->d != -1){
      v2->Cells.push_back(c);
    }
  }
  for (auto &e_: v1->Edges){
    if (e_->usable == true){
      v2->Edges.push_back(e_);
    }
  }
  for (auto &e_ : edges){
    for (auto c =  e_->Cells.begin(); c != e_->Cells.end();c++){
      if ((*c)->d == -1){
	e_->Cells.erase(c--);
      }
    }
  }
  for (auto &c:v2->Cells){
    if (c->d ==-1){ cout<<"wrong Cell!"<<endl;}
    for (auto &e_:c->Edges ){
      if (e_->usable ==false){cout<<"wrong Edge!"<<endl;}
      if (e_->vert1->index ==-1 || e_->vert2->index ==-1 ){
	cout<<"wrong Vert2!"<<endl;
      }
    }
    for (auto &v: c->Vertices ){
      if (v->index ==-1){cout<<"wrong Vertex!"<<endl;}
      for (auto &e2_: v->Edges){
	if (e2_->usable ==false){cout<<"wrong Edge 2!"<<endl;}
      }
      
    }
  }
  for (auto &c :v2->Cells ){
    for (auto &v : c->Vertices){
      if (v->index ==-1){
	v = v2;
	//cout<<"here \n";
      }
    }
  }
  //cout<<"number of cells in v2:"<<v2->Cells.size()<<endl;
  //cout<<"number of edges in v2:"<<v2->Edges.size()<<endl;
  /*
  cout<<"# cells :"<<v2->Cells.size()<<endl;
  for (auto &c: v2->Cells){
    cout<<"#vertices = "<< c->Vertices.size()<<"#edges = "<<c->Edges.size()<<endl;
  }
  for (auto &e_:v2->Edges){
    cout<<"#cells "<<e_->Cells.size()<<endl;
  }
  */
}


bool Mesher::is_starshaped_polygon(vertex *v_mid,vertex * v_center){
  double area_old = 0.0,area_new = 0.0,area_c;
  double x_center = v_center->x,y_center= v_center->y;
  vector<edge*> edges; 
  for(auto &c:v_mid->Cells){
    area_old += area(c);
    for(auto &e:c->Edges){
      if (e->vert1 != v_mid && e->vert2 != v_mid){
        edges.push_back(e);
      }
    }
  }
  for (auto &e: edges){
    if (e->vert1 != v_center && e->vert2 != v_center){
      area_c = area_(x_center,y_center,e->vert1->x,e->vert1->y,e->vert2->x,e->vert2->y);
      if (area_c<area_old*0.05){
        return false;
      }else{
        area_new += area_c;
      }
    }
  }
  if (abs(area_old-area_new) < area_old*0.0001){
    return true;
  }else{
    return false;
  }
}


