#include "mesher.h"

//compute cell quality parameter  (inner circle)/(triangle area)
double Mesher::d(cell* cell){
  double x1 = cell->Vertices[0]->x;
  double y1 = cell->Vertices[0]->y;
  double x2 = cell->Vertices[1]->x;
  double y2 = cell->Vertices[1]->y;
  double x3 = cell->Vertices[2]->x;
  double y3 = cell->Vertices[2]->y;
  double a = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
  double b = sqrt(pow((x1-x3),2) + pow((y1-y3),2));
  double c = sqrt(pow((x2-x3),2) + pow((y2-y3),2));
  double s = (a+b+c)/2;
  double r = sqrt(abs((s-a)*(s-b)*(s-c)/s));
  double S = 0.5*abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1));
  if(S <pow(s,2)*0.001){
    return 0.;
  }else{
    return pi*(r*r)/S;
  } 
}
double Mesher::area(cell* cell){
  double x1 = cell->Vertices[0]->x;
  double y1 = cell->Vertices[0]->y;
  double x2 = cell->Vertices[1]->x;
  double y2 = cell->Vertices[1]->y;
  double x3 = cell->Vertices[2]->x;
  double y3 = cell->Vertices[2]->y;
  return 0.5*abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1));
}
double Mesher::area_(double x1,double y1,double x2,double y2,double x3,double y3){
  return  0.5*abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1));
}

double Mesher::max_edge(cell* cell){
  double x1 = cell->Vertices[0]->x;
  double y1 = cell->Vertices[0]->y;
  double x2 = cell->Vertices[1]->x;
  double y2 = cell->Vertices[1]->y;
  double x3 = cell->Vertices[2]->x;
  double y3 = cell->Vertices[2]->y;
  double len_edge1 = pow(x1-x2,2) + pow(y1-y2,2) ;
  double len_edge2 = pow(x2-x3,2) + pow(y2-y3,2) ;
  double len_edge3 = pow(x1-x3,2) + pow(y1-y3,2) ;
  return max(len_edge1,max(len_edge2,len_edge3)) ;
}

double Mesher::r(cell* cell){
  double x1 = cell->Vertices[0]->x;
  double y1 = cell->Vertices[0]->y;
  double x2 = cell->Vertices[1]->x;
  double y2 = cell->Vertices[1]->y;
  double x3 = cell->Vertices[2]->x;
  double y3 = cell->Vertices[2]->y;
  double a = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
  double b = sqrt(pow((x1-x3),2) + pow((y1-y3),2));
  double c = sqrt(pow((x2-x3),2) + pow((y2-y3),2));
  double s = (a+b+c+0.000001)/2;
  return sqrt(abs((s-a)*(s-b)*(s-c)/s));
}

double Mesher::r_(double x1,double y1,double x2,double y2,double x3,double y3){
  double a = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
  double b = sqrt(pow((x1-x3),2) + pow((y1-y3),2));
  double c = sqrt(pow((x2-x3),2) + pow((y2-y3),2));
  double s = (a+b+c+0.000001)/2;
  return sqrt(abs((s-a)*(s-b)*(s-c)/s));  
}
double Mesher::d_(double x1,double y1,double x2,double y2,double x3,double y3){
  double a = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
  double b = sqrt(pow((x1-x3),2) + pow((y1-y3),2));
  double c = sqrt(pow((x2-x3),2) + pow((y2-y3),2));
  double s = (a+b+c+0.000001)/2;
  double r = sqrt(abs((s-a)*(s-b)*(s-c)/s));
  double S = 0.5*abs((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1));
  if(S ==0.){
    return 0.;
  }else{
    return pi*(r*r)/S;
  } 
}

double Mesher::edge_len(edge *e){
  vertex *v1 = e->vert1;
  vertex *v2 = e->vert2;
  return sqrt(pow((v1->x - v2->x),2) + pow((v1->y - v2->y),2));
}

double Mesher::edge_param(edge *e){
  double p1 = e->vert1->remesh_param;
  double p2 = e->vert2->remesh_param;
  return 0.5*(p1+p2);
}

double Mesher::cell_param(cell*c){
  if (c->Vertices.size() ==3){
    return (c->Vertices[0]->remesh_param + c->Vertices[1]->remesh_param +c->Vertices[2]->remesh_param)/3;
  }else{
    throw std::runtime_error("Cell with wrong number of vertices. Error in parameters.cpp in cell_param.");
    return -1.0;
  }
}
