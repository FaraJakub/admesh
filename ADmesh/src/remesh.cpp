#include "mesher.h"

# define C           3.464101615
//# define C           2.464101615
# define line "-------------------------------------------------- \n"
void Mesher::remesh(double r_min,double r_max,int num_iter,bool Marge_cells,bool Add_Vertex_On_Edge,
		    bool  Flip_Edges,bool Remove_Bndry_Edge,bool Remove_Vertices,bool Move_Vertices,bool Report){
  double e_min = C*r_min;
  double e_max = C*r_max;
  //int i = 0;
  
  double x1,x2,x3,x4,y1,y2,y3,y4,r1,r2,area0,area1,area2,cos_alpha,cos_beta,cos_gamma;
  long unsigned int len;
  
  int array_marge_cells[num_iter];
  int array_add_vertex_on_edge[num_iter];
  int array_flip_edges[num_iter];
  int array_remove_bndry_edge[num_iter];
  int array_remove_vertex[num_iter];
  
  int num_marge_cells = 0;
  int num_add_vertex_on_edge = 0;
  int num_flip_edges = 0;
  int num_remove_bndry_edge = 0;
  int num_remove_vertex = 0;

  
  vector<edge*>::iterator end;
  edge *e;
  vertex *v;
  cell *c;
  int k = 0;
  //print_wrong_entities("0");
  
  for (int j = 0;j<num_iter;j++){
    num_marge_cells = 0;
    num_add_vertex_on_edge = 0;
    num_flip_edges = 0;
    num_remove_bndry_edge = 0;
    num_remove_vertex = 0;

    
    if (Marge_cells == true){
      for (int k=0;k<Edges.size();k++){
	edge *e = Edges[k];
	if (e->usable ==true && e->vert1->bndry == false && e->vert2->bndry == false
	    && e->vert1->interface == e->vert2->interface){
	  if (edge_len(e)<e_min*edge_param(e)){
	    marge_cells_i(k);
	    num_marge_cells+=1;
	  }
	}
      }
    }


    len= Edges.size();
    if (Add_Vertex_On_Edge == true){
      for (int i = 0;i<len;i++){
	e = Edges[i];
	if (e->usable == true){
	  if ( edge_len(e)>edge_param(e)*e_max){
	    add_vert_edge_i(i);
	    num_add_vertex_on_edge +=1;
	  }
	}
      }
      //     end = Edges.end();
    }
    for (auto e = Edges.begin(); e!= Edges.end();e++){
      if ((*e)->usable == false){
	delete (*e);
	Edges.erase(e--);
      }
    }
    //print_wrong_entities("0");

     
    
    len = Edges.size();
    
    if (Remove_Bndry_Edge==true || Flip_Edges ==true){
      for (long unsigned int i = 0;i<len;i++){
	e = Edges[i];
	if (e->usable == true){
	  if (Flip_Edges == true &&e->bndry ==false&&e->interface ==false){
	    x1 = e->vert1->x;
	    y1 = e->vert1->y;
	    x2 = e->vert2->x;
	    y2 = e->vert2->y;
	    for (auto &v: e->Cells[0]->Vertices){
	      if (v!=e->vert1 &&v!=e->vert2){
		y3 = v->y;
		x3 = v->x;
	      }
	    }
	    for (auto &v: e->Cells[1]->Vertices){
	      if (v!=e->vert1 &&v!=e->vert2){
		y4 = v->y;
		x4 = v->x;
	      }
	    }
	    r1 = min(d_(x1,y1,x2,y2,x3,y3),d_(x1,y1,x2,y2,x4,y4));
	    r2 = min(d_(x1,y1,x4,y4,x3,y3),d_(x3,y3,x2,y2,x4,y4));
	    area0 = area(e->Cells[0])+ area(e->Cells[1]);
	    area1 = area_(x1,y1,x4,y4,x3,y3);
	    area2 = area_(x2,y2,x4,y4,x3,y3);
	    
	    if (r2>r1+0.0001 && area0*0.9>area1 && area0*0.9>area2 &&
		area1>area0*0.1 && area2>area0*0.1  ){
	      flip_edge(e);
	      num_flip_edges +=1;
	    }
	  }else if(Remove_Bndry_Edge == true && e->bndry ==true &&e->Cells.size() == 1 &&
		   edge_len(e)<e_min*edge_param(e)){
	    remove_bndry_edge_i(i);
	    num_remove_bndry_edge +=1;
	  }
	}
      }
    }
    //print_wrong_entities("1");
    len = Cells.size();
    if(Remove_Vertices ==true){
      for (long unsigned int i = 0;i<len;i++){
	c = Cells[i];
	if (c->d != -1){
	  bool bndry = true;
	  for (auto &e:c->Edges ){
	    if (e->bndry  == false ||e->interface  == false){
	      bndry = false;
	      break;
	    }
	  }
	  if ( r(c)<r_min*cell_param(c) && bndry ==false){
	    num_remove_vertex +=1;
	    x1 = c->Vertices[0]->x;
	    y1 = c->Vertices[0]->y;
	    x2 = c->Vertices[1]->x;
	    y2 = c->Vertices[1]->y;
	    x3 = c->Vertices[2]->x;
	    y3 = c->Vertices[2]->y;
	    if ((c->Vertices[0]->bndry==false &&
		 c->Vertices[0]->interface==false)){
	      cos_alpha = cosinus(x2-x1,y2-y1,x3-x1,y3-y1);
	    }else{
	      cos_alpha = 1.;
	    }
	    if(c->Vertices[1]->bndry==false &&
	       c->Vertices[1]->interface==false){
	      cos_beta = cosinus(x1-x2,y1-y2,x3-x2,y3-y2);
	    }else{
	      cos_beta = 1.;
	    }
	    if(c->Vertices[2]->bndry==false  &&
	       c->Vertices[2]->interface==false){
	      cos_gamma = cosinus(x1-x3,y1-y3,x2-x3,y2-y3);
	    }else{
	      cos_gamma = 1.;
	    }
	    if (min(min(cos_alpha,cos_beta),cos_gamma)<1){
	      if (cos_alpha<cos_beta){
		if (cos_alpha<cos_gamma){
		  remove_vertex(c->Vertices[0]);
		}else{
		  remove_vertex(c->Vertices[2]);
		}
	      }else{
		if(cos_beta<cos_gamma){
		  remove_vertex(c->Vertices[1]);
		}else{
		  remove_vertex(c->Vertices[2]);
		}
	      }
	    }
	  }
	}
      }
    }
    //print_wrong_entities("2");
    if (Move_Vertices ==true){
      move_vertices();
    }
    
    //print_wrong_entities("3");

    array_flip_edges[j] = num_flip_edges;
    array_remove_vertex[j] = num_remove_vertex;
    array_marge_cells[j] = num_marge_cells;
    array_add_vertex_on_edge[j] = num_add_vertex_on_edge;
    array_remove_bndry_edge[j] = num_remove_bndry_edge;
  }
  if (Report ==true){
    print_wrong_entities(" ");
    for (int j =0;j<num_iter;j++){
      cout<<line;
      cout<<"#flipped edges: "<<array_flip_edges[j]<<endl;
      cout<<"#removed vertices: "<<array_remove_vertex[j]<<endl;
      cout<<"#removed boundary edges: "<<array_remove_bndry_edge[j]<<endl;
      cout<<"#marged cells: "<<array_marge_cells[j]<<endl;
      cout<<"#add vertices on edge: "<<array_add_vertex_on_edge[j]<<endl;
    }
    cout<<"#Cell:"<<Cells.size()<<endl;
    cout<<"#Edges:"<<Edges.size()<<endl;
    cout<<"#Vertices:"<<Vertices.size()<<endl;
  }
}

double Mesher::cosinus(double ax,double ay,double bx,double by){
  return (ax*bx + ay*by)/(sqrt(ax*ax+ay*ay)*sqrt(bx*bx+by*by));
}
