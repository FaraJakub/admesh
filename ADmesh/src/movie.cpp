#include "mesher.h"

# define C           3.464101615

int Mesher::make_one_step(double r_min,double r_max,int start,string operation){
  double e_min = C*r_min;
  double e_max = C*r_max;
  edge *e;
  int len,k;
  if (operation == "marge_cells"){
    for (k=start;k<Edges.size();k++){
      edge *e = Edges[k];
      if (e->usable ==true &&
	  e->Cells.size()==2 &&
	  e->vert1->bndry ==false &&
	  e->vert2->bndry ==false &&
	  e->Cells[0]->bndry == false &&
	  e->Cells[1]->bndry == false ){
	if (edge_len(e)<e_min*edge_param(e)){
	  marge_cells_i(k);
	  return k;
	}
      }
    }
    print_wrong_entities("marge_cells");
  }else if (operation == "add_vertex_on_edge"){
    len= Edges.size();
    
    for (k = start;k<len;k++){
      e = Edges[k];
      if (e->usable == true){
	if ( edge_len(e)>edge_param(e)*e_max){
	  add_vert_edge_i(k);
	  return(k);
	}
      }
    }
    print_wrong_entities("add_vertex_on_edge");
  }else if (operation == "flip_edge"){
    len = Edges.size();
    double x1,x2,x3,x4,y1,y2,y3,y4,r1,r2,area1,area2;

    for (k = start;k<len;k++){
      e = Edges[k];
      if ( e->usable == true &&e->bndry ==false){
	x1 = e->vert1->x;
	y1 = e->vert1->y;
	x2 = e->vert2->x;
	y2 = e->vert2->y;
	for (auto &v: e->Cells[0]->Vertices){
	  if (v!=e->vert1 &&v!=e->vert2){
	    y3 = v->y;
	    x3 = v->x;
	  }
	}
	for (auto &v: e->Cells[1]->Vertices){
	  if (v!=e->vert1 &&v!=e->vert2){
	    y4 = v->y;
	    x4 = v->x;
	  }
	}
	r1 = min(d_(x1,y1,x2,y2,x3,y3),d_(x1,y1,x2,y2,x4,y4));
	r2 = min(d_(x1,y1,x4,y4,x3,y3),d_(x3,y3,x2,y2,x4,y4));
	area1 = area_(x1,y1,x2,y2,x3,y3) + area_(x1,y1,x2,y2,x4,y4);
	area2 = area_(x1,y1,x4,y4,x3,y3) + area_(x3,y3,x2,y2,x4,y4);
	
	if (r2>r1+0.000001 && abs(area1- area2)<0.000001){
	  flip_edge(e);
	  return k+1;
	}
      }/*else if( e->usable == true && e->bndry ==true &&e->Cells.size() == 1 && edge_len(e)<e_min*edge_param(e)){
	remove_bndry_edge_i(k);
	cout<<" k be = "<<k<<endl;
	return k;
	}*/
    }
    print_wrong_entities("flip_edge");
  }else if(operation == "remove_vertex"){
    cell *c;
    bool bndry;
    double cos_alpha,cos_beta,cos_gamma,x1,x2,x3,y1,y2,y3;
    len = Cells.size();
    for (k = start; k<len; k++){
      c = Cells[k];
      if (c->d != -1){
	bndry = true;
	for (auto &e:c->Edges ){
	  if (e->bndry  == false){
	    bndry = false;
	    break;
	  }
	}
	if ( r(c)<r_min*cell_param(c) && bndry ==false){
	  x1 = c->Vertices[0]->x;
	  y1 = c->Vertices[0]->y;
	  x2 = c->Vertices[1]->x;
	  y2 = c->Vertices[1]->y;
	  x3 = c->Vertices[2]->x;
	  y3 = c->Vertices[2]->y;
	  if ((c->Vertices[0]->bndry==false)){
	    cos_alpha = cosinus(x2-x1,y2-y1,x3-x1,y3-y1);
	  }else{
	    cos_alpha = 1.;
	  }
	  if(c->Vertices[1]->bndry==false){
	    cos_beta = cosinus(x1-x2,y1-y2,x3-x2,y3-y2);
	  }else{
	    cos_beta = 1.;
	  }
	  if(c->Vertices[2]->bndry==false){
	    cos_gamma = cosinus(x1-x3,y1-y3,x2-x3,y2-y3);
	  }else{
	    cos_gamma = 1.;
	  }
	  if (min(min(cos_alpha,cos_beta),cos_gamma)<1){
	    if (cos_alpha<cos_beta){
	      if (cos_alpha<cos_gamma){
		remove_vertex(c->Vertices[0]);
		return k+1;
	      }else{
		remove_vertex(c->Vertices[2]);
		return k+1;
	      }
	    }else{
	      if(cos_beta<cos_gamma){
		remove_vertex(c->Vertices[1]);
		return k+1;
	      }else{
		remove_vertex(c->Vertices[2]);
		return k+1;
	      }
	    }
	  }
	}
      }
    }
  }
  
  return -1;

}
