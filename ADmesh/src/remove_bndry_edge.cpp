#include "mesher.h"

void Mesher::remove_bndry_edge_i(int index){
  edge *e = Edges[index];
  if (e->usable ==true){
    remove_bndry_edge(e);
  }else{cout<<"Warning you tried to marge cells of unusable edge"<<endl;}

}

void Mesher::remove_bndry_edge(edge * e){
  cell *c = e->Cells[0];
  edge *eb1,*eb2,*e1,*er;
  vertex *vr,*v1,*v2,*vb;
  //remove the vertex with shorter boundary edge
  for (auto &e_: e->vert1->Edges){
    if(e_->bndry == true && e_!=e ){
      eb1 = e_;
      break;
    }
  }
  for (auto &e_: e->vert2->Edges){
    if(e_->bndry == true && e_!=e ){
      eb2 = e_;
      break;
    }
  }
  if (edge_len(eb1)>edge_len(eb2)){
    v1 = e->vert1;
    vr = e->vert2;
    if (eb2->vert1 != vr){
      vb = eb2->vert1;
    }else{ vb = eb2->vert2;}
  }else{
    v1 = e->vert2;
    vr = e->vert1;
    if (eb1->vert1 != vr){
      vb = eb1->vert1;
    }else{ vb = eb1->vert2;}
  }
  //____________________________________________

  for (auto &v : c->Vertices){
    if (v != v1 && v !=vr){
      v2 = v;
      break;
    }
  }
  for (auto &e_: c->Edges){
    if (e_!=e){
      if (e_->vert1 == v1 ||e_->vert2 == v1){
	e1 = e_;
      }
      else if (e_->vert1 == vr ||e_->vert2 == vr){
	er = e_;
      }
    } 
  }
  if (is_starshaped_polygon(v1,vr) == false){
    return;
  }
  double ax = vr->x - v1->x;
  double ay = vr->y - v1->y;
  double bx = vr->x - vb->x;
  double by = vr->y - vb->y;
  
  if (cosinus(ax,ay,bx,by)>-0.92){
    //cout<<"return \n";
    return;
  }

  //cout<<"("<<er->vert1->x<<" , "<<er->vert1->y<<") ("<<er->vert2->x<<" , "<<er->vert2->y<<")"<<endl;
  //cout<<er->Cells.size()<<endl;
  vr->index = -1;
  c->d = -1;

  er->usable = false;
  e->usable = false;
  //remove c from {v1,v2}
  vector<vertex*> vertices ={v1,v2}; 
  for (auto &v: vertices){
    for (auto c_= v->Cells.begin();c_!= v->Cells.end();c_++){
      if ((*c_)->d ==-1){
	v->Cells.erase(c_--);
      }
    }
  }
  for (auto &v: vertices){
    for (auto e_= v->Edges.begin();e_!= v->Edges.end();e_++){
      if ((*e_)->usable ==false){
	v->Edges.erase(e_--);
      }
    }
  }

  for (auto c_= e1->Cells.begin();c_!= e1->Cells.end();c_++){
    if ((*c_)->d ==-1){
      e1->Cells.erase(c_--);
      break;
    }
  }
  for (auto &c_:vr->Cells){
    if (c_->d !=-1){
      for (auto &v_:c_->Vertices){
	if (v_ == vr){
	  v_ = v1;
	}
      }
      for (auto &e_: c_->Edges){
	if (e_->usable ==false){
	  e_ = e1;
	  break;
	}
      }
      v1->Cells.push_back(c_);
    }
  }
  for (auto &c_: er->Cells){
    if (c_ != c){
      e1->Cells.push_back(c_);
    }
  }
  for (auto &e_:vr->Edges){
    if (e_->usable ==true){
      v1->Edges.push_back(e_);
    }
    if (e_->vert1 == vr){
      e_->vert1 = v1;
    }else if (e_->vert2 == vr){
      e_->vert2 = v1;
    } 
  }
  //for (auto c_:)
}
