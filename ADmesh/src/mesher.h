#ifndef MESHER_H
#define MESHER_H
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>  

# define pi           3.14159265358979323846 
namespace py = pybind11;
using namespace std;

struct vertex;
struct edge;

struct cell{
  vector<vertex*> Vertices;
  vector<edge*> Edges;
  double d = 1; //quality parameter
  int mark = 0;
  bool bndry = false;
};

struct vertex{
  double x;
  double y;
  double remesh_param = 1.0;
  int index = 1;
  bool bndry = false;
  bool interface = false;
  vector<cell*> Cells;
  vector<edge*> Edges;
};

struct edge{
  vertex *vert1;
  vertex *vert2;
  vector<cell*> Cells;
  //cell *cell1;
  //cell *cell2;
  bool usable = true;
  bool interface = false;
  bool bndry = false;
};

//,const bool remove_vert = true,const bool add_vert = true,const bool move = true 
class Mesher {
public:
  Mesher(py::array_t<double> Coordinates, py::array_t<int> Cells_arr,py::array_t<int> Edge_arr);
  void repair_mesh(const double min_r,const double max_r);
  void remove_vertex(vertex* vert);
  void remove_vertex_i(int index);
  void add_vert_edge(edge* e);
  void add_vert_edge_i(int index);
  void refine_cell(cell* c);
  void flip_edge(edge*);
  void flip_edge_i(int);
  void marge_cells(edge *);
  void marge_cells_i(int);
  void remove_bndry_edge(edge*);
  void remove_bndry_edge_i(int);
  void remesh(double ,double,int = 4,bool = true,bool = true,
	      bool = true,bool = true,bool = true,bool = true,bool = true);
  py::array_t<double> getCoordinates();
  py::array_t<double> getBoundaryCoordinates();
  py::array_t<int> getCells();
  py::array_t<int> getMarks();
  double get_rmin();
  double get_rmax();
  int num_of_vertices();
  void move_mesh(py::array_t<double> Coordinates);
  void move_vertices(); //move vertices to baricentric point
  void set_boundary(py::array_t<int> Indices);
  void set_marks(const py::array_t<int> cls,const int value);
  void set_remesh_params(const py::array_t<double> );
  void change_coordinates(const py::array_t<double> new_coor);
  void set_outer_bndry();
  void set_inner_bndry();
  int make_one_step(double,double,int,string);
private:
  bool is_valid_edge(edge*);
  bool is_convex(vertex*);
  bool is_starshaped_polygon(vertex*,vertex*);
  vector<cell*> make_cells(vector<vertex*> circle,vector<cell*> new_cells,double& p,vector<cell*> &all_cells );
  double d(cell* cell);
  double d_(double,double,double,double,double,double);
  double r(cell* cell);
  double r_(double,double,double,double,double,double);
  double area(cell* cell);
  double area_(double,double,double,double,double,double);
  double max_edge(cell* cell);
  double edge_len(edge*);
  double edge_param(edge*);
  double cosinus(double,double,double,double);
  double cell_param(cell*);
  void create_cell(edge *e1,edge *e2,edge *e3,vertex *v1,vertex *v2,vertex *v3,int mark,int d_ = 1);
  void add_edges_to_cell(cell*);
  vector<cell*> Cells;
  vector<vertex*> Vertices;
  vector<edge*> Edges;
  void print_wrong_entities(string);
};

#endif
