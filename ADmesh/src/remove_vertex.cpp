#include "mesher.h"

void add_edges_to_cell(cell*);

void Mesher::remove_vertex_i(int index){
  vertex* v = Vertices[index];
  remove_vertex(v);
}

void Mesher::remove_vertex(vertex * vert){
  if (is_convex(vert) == false){
    return;
  }
  vector<cell*> cls = vert->Cells;
  vert->index = -1; //labeling vertex which is no longer usable
  int num_cls = cls.size();
  int mark = cls[0]->mark;
  //firt element of circle
  vertex* last = new vertex;
  vector<vertex*> circle;
  if (cls[0]->Vertices[0]->index != -1){
    last = cls[0]->Vertices[0];
    circle.push_back(last);
  }else{
    last = cls[0]->Vertices[1];
    circle.push_back(last);
  }
  /*
  if (cls.size() == 7){
    for (auto &c :vert->Cells){
      cout<<"c->d = "<<c->d<<endl;
      cout<<c<<endl;
      
      for(auto &v: c->Vertices){
	cout<<" ("<<v->x<<", "<<v->y<<" ) ";
	cout<<v<<endl;
      }
      cout<<"\n";
    }
  }
  */
  //make the circle
  for (auto & c :cls){c->d =-1;} //This cell is no longer usable
  for (auto & e :vert->Edges){e->usable = false;}//These edges will be removed
  for(int i =0;i<num_cls-1;i++){
      for (auto c = cls.begin(); c != cls.end(); c++)  {
	if (find((*c)->Vertices.begin(), (*c)->Vertices.end(), last) != (*c)->Vertices.end()){
	    for(int j = 0;j<3;j++){
	      if ((*c)->Vertices[j] != last && (*c)->Vertices[j]->index != -1){
		last = (*c)->Vertices[j];
		circle.push_back(last);
		cls.erase(c--);
		break;
	      }
	    }
	    break;
	  }
      }
  }
  vector<cell*>  new_cells;
  double p= 1.;
  //cout<<"vert->cells.size()"<<vert->Cells.size()<<endl;
  //cout<<"circle.size() = "<<circle.size()<<endl;
  //cout<<"-> ("<<vert->x<<", "<<vert->y<<") \n";
  /*
  if (circle.size() == 7){
    for (auto &v:circle){
      cout<<" ("<<v->x<<", "<<v->y<<") \n";
    }
    }*/
  if (p ==0 || circle.size()<3){
    cout<<"removing uncussesful \n";
    vert->index = 1;
    for (auto & c :vert->Cells){c->d =1;}
    for (auto & e :vert->Edges){
      e->usable = true;
      cout<<" ("<<e->vert1->x<<", "<<e->vert1->y<<")  ("<<e->vert2->x<<", "<<e->vert2->y<<") \n";
    }
    return;
  }
  vector<cell*> all_cells;
  new_cells = make_cells(circle,new_cells,p,all_cells);
  for (auto & c: new_cells){
    c->d = 1;
  }
  for (auto & c: all_cells){
    if (c->d != 1){
      delete c;
    }
  }
  for (auto &v: circle){
    for (auto e = v->Edges.begin();e!= v->Edges.end();e++){
      if ((*e)->usable == false){
	v->Edges.erase(e--);
      }
    }
  }

  //cout<<" p = "<<p<<endl;
  Cells.insert( Cells.end(), new_cells.begin(), new_cells.end());
  for (auto & c : new_cells){
    c->mark = mark;
    for (auto & v : c->Vertices){
      for (auto cl = v->Cells.begin(); cl != v->Cells.end(); cl++) {
	if ((*cl)->d ==-1){
	  v->Cells.erase(cl--);
	}
      }
      v->Cells.push_back(c);
    }
  }
  for (auto &c : new_cells){
    for (auto v = c->Vertices.begin(); v != c->Vertices.end(); v++){
      if ((*v)->index ==-1){
	c->Vertices.erase(v--);
      }
    }
  }
  for (auto &c : new_cells){
    for(auto e= c->Edges.begin();e!=c->Edges.end();e++){
      if ((*e)->usable ==false){
	c->Edges.erase(e--);
      }
      for (auto c_ = (*e)->Cells.begin(); c_!= (*e)->Cells.end();c_++){
	if ((*c_)->d==-1){
	  (*e)->Cells.erase(c_--);
	}
      }
    }
  }
  for (auto &c : new_cells){add_edges_to_cell(c);}
  for (auto &c : new_cells){
    //cout<<"size of new_cells = "<<new_cells.size()<<endl;
    //if (c->Vertices.size()!=3){
    //  cout<<"number of Verices in cell is "<<c->Vertices.size()<<endl;
    //}
    //for (auto &v: c->Vertices){
    //  cout<<"   number of edges = "<<v->Edges.size()<<"v->index = "<<v->index<<endl;
    //}
    if (c->d ==-1 ){
      cout<<"here"<<endl;
    }
    for(auto &e: c->Edges){
      //cout<<e->vert1->index<<endl;
      //cout<<e->vert2->index<<endl;
      if (e->Cells.size() != 2 && e->bndry == false){
	cout<<"vert = ("<<vert->x<<", "<<vert->y<<") \n";
	cout<<"   number of Cells in edge is "<<e->Cells.size()<<endl;
	cout<<"edge = ("<<e->vert1->x<<", "<<e->vert1->y<<"), ("<<e->vert2->x<<", "<<e->vert2->y<<") \n";
	for (auto &e_: e->vert1->Edges){
	  cout<<"       edge = ("<<e_->vert1->x<<", "<<e_->vert1->y<<"), ("<<e_->vert2->x<<", "<<e_->vert2->y<<") \n";
	}
	for (auto &e_: e->vert2->Edges){
	  cout<<"       edge = ("<<e_->vert1->x<<", "<<e_->vert1->y<<"), ("<<e_->vert2->x<<", "<<e_->vert2->y<<") \n";
	}
      }
    }
  }
  //for (auto &e :circle){
  //  cout<<e->index<<endl;
  //}
}

vector<cell*> Mesher::make_cells(vector<vertex*> circle,vector<cell*> new_cells,double& p, vector<cell*>& all_cells){
  if (circle.size() == 3){
    cell* c = new cell;
    //cout<<"c was crated "<<c<<endl;
    c->Vertices.push_back(circle[0]);
    c->Vertices.push_back(circle[1]);
    c->Vertices.push_back(circle[2]);
    //cout<<"c0->Edges.size() = "<<circle[0]->Edges.size()<<endl;
    //cout<<"c0->Cells.size() = "<<circle[0]->Cells.size()<<endl;
    c->d = d(c);
    if (c->d < p){p=c->d;}
    new_cells.push_back(c);
    all_cells.push_back(c);
    //for (auto &c : new_cells){cout<<c<<", ";}
    //cout<<endl;
    //cout<<" c->d = "<<c->d<<" \n ";
    return new_cells;
  }else{
    int len = circle.size();
    double p_best = 0;
    vector<cell*> best_cells;
    //try all possible cells, when circle[0] has additional edge
    for (int i =2;i<len-1 ;i++){
      //vector<double*>::const_iterator first = myVec.begin() + 100000;
      //cout<<"circle len = "<<circle.size()<<"\n";
      vector<vertex*> subvector1(circle.begin(), circle.begin() + i+1);
      vector<vertex*> subvector2(circle.begin()+i, circle.end());
      subvector2.push_back(circle[0]);
      //cout<<"len1 = "<<subvector1.size()<<" len2 = "<<subvector2.size()<<" \n";
      vector<cell*> temp_cells;
      double p_new = p;
      temp_cells = make_cells(subvector1,new_cells,p_new,all_cells);
      temp_cells = make_cells(subvector2,temp_cells,p_new,all_cells);
      if(p_new>p_best){
	p_best = p_new;
	best_cells = temp_cells;
      }/*else{
	cout<<"size = "<<temp_cells.size()<<" size_new_cells = "<<new_cells.size()<<" depth = "<<depth<<endl;
	if (temp_cells.size() != 0){
	  for (auto c = temp_cells.begin()+new_cells.size(); c != temp_cells.end(); c++){
	    if (*c != nullptr){
	      cout<<*c<<" "<<endl;
	      delete *c;}
	  }
	  temp_cells.clear();
	  cout<<"here 3 \n";
	}
	}*/
    }
    //case when circle[0] has no additional edge
    vector<vertex*> subvector1(circle.begin()+1, circle.end());
    vector<vertex*> subvector2(circle.begin(),circle.begin()+2);
    subvector2.push_back(circle.back());
    vector<cell*> temp_cells;
    double p_new = p;
    temp_cells = make_cells(subvector1,new_cells,p_new,all_cells);
    temp_cells = make_cells(subvector2,temp_cells,p_new,all_cells);
    if(p_new>p_best){
      p_best = p_new;
      best_cells = temp_cells;
    }
    p = p_best;
    //cout<<"p_best = "<<p_best<<" \n";
    //cout<<"best cells \n";
    return best_cells;
  }
}

void Mesher::add_edges_to_cell(cell* c){
  vertex *v0 = c->Vertices[0];
  vertex *v1 = c->Vertices[1];
  vertex *v2 = c->Vertices[2];
  if (v1 == v0 || v1 == v2 || v2 == v0){cout<<"problem!!"<<endl;}
  //cout<<"v->Edges.size() = "<<v0->Edges.size()<<" "<<v1->Edges.size()<<" "<<v2->Edges.size()<<endl;
  //v0--v1 edge
  bool edge_exist = false; 
  for (auto *e : v0->Edges){
    //cout<<"("<<e->vert1->x<<" , "<<e->vert1->y<<")"<<endl;
    if ((e->vert1 == v1) || (e->vert2 == v1)){
      c->Edges.push_back(e);
      e->Cells.push_back(c);
      edge_exist = true;
      for (auto c_= e->Cells.begin();c_!= e->Cells.end();c_++){
	if ((*c_)->d ==-1){
	  e->Cells.erase(c_--);
	}
      }
      if (e->Cells.size()>2){cout<<"error 1";}
      break;
    }
  }
  if (edge_exist == false){
    edge *e = new edge;
    e->Cells.push_back(c);
    e->vert1 = v0;
    e->vert2 = v1;
    if (v0->bndry == true && v1->bndry == true ){
      cout<<"edge = ("<<e->vert1->x<<", "<<e->vert1->y<<"), ("<<e->vert2->x<<", "<<e->vert2->y<<") \n";
    }
    c->Edges.push_back(e);
    v0->Edges.push_back(e);
    v1->Edges.push_back(e);
    Edges.push_back(e);
  }
  //v0--v2 edge
  edge_exist = false; 
  for (auto *e : v0->Edges){
    if ((e->vert1 == v2) || (e->vert2 == v2)){
      c->Edges.push_back(e);
      e->Cells.push_back(c);
      edge_exist = true;
      for (auto c_= e->Cells.begin();c_!= e->Cells.end();c_++){
	if ((*c_)->d ==-1){
	  e->Cells.erase(c_--);
	}
      }
      if (e->Cells.size()>2){cout<<"error 2";}
      break;
    }
  }
  if (edge_exist == false){
    edge *e = new edge;
    e->Cells.push_back(c);
    e->vert1 = v0;
    e->vert2 = v2;
    c->Edges.push_back(e);
    v0->Edges.push_back(e);
    v2->Edges.push_back(e);
    Edges.push_back(e);
  }
  //v1--v2 edge
  edge_exist = false; 
  for (auto *e : v1->Edges){
    if ((e->vert1 == v2) || (e->vert2 == v2)){
      c->Edges.push_back(e);
      e->Cells.push_back(c);
      edge_exist = true;
      for (auto c_= e->Cells.begin();c_!= e->Cells.end();c_++){
	if ((*c_)->d ==-1){
	  e->Cells.erase(c_--);
	}
      }
      /*
      if (e->Cells.size()>2){
	cout<<"error 3 \n";
	cout<<v0->x<<" \n";
	cout<<v1->x<<" \n";
	cout<<v2->x<<" \n";
	for (auto &cl: e->Cells){
	  cout<<cl->d<<endl;
	  for (auto &ve: cl->Vertices ){
	    cout<<ve->x<<" "<<ve->y<<" ";
	  }
	  cout<<endl;
	}
      }
      break;
      */
    }
  }
  if (edge_exist == false){
    edge *e = new edge;
    e->Cells.push_back(c);
    e->vert1 = v1;
    e->vert2 = v2;
    c->Edges.push_back(e);
    v1->Edges.push_back(e);
    v2->Edges.push_back(e);
    Edges.push_back(e);
  }
}

bool Mesher::is_convex(vertex *v){
  if(v->Cells.size() == 3){
    return true;
  }
  edge *e =  v->Edges[0];;
  cell *c1 = e->Cells[0];
  cell *c2 = e->Cells[1];
  vertex *v0,*v1,*v2;
  if (e->vert1 != v){
    v0 = e->vert1;
  }else{
    v0 = e->vert2;
  }
  for (auto &v_ : c1->Vertices){
    if (v_ != v0 && v_ != v){
      v1 = v_;
      break;
    }
  }
  for (auto &v_ : c2->Vertices){
    if (v_ != v0 && v_ != v){
      v2 = v_;
      break;
    }
  }


  double area0,area1,area2;
  for (int i = 0; i< v->Edges.size(); i++){
    if (e->vert1 != v){
      v1 = e->vert1;
    }else{
      v1 = e->vert2;
    }
    area1 = area(c1);
    area2 = area(c2);
    area0 = area_(v1->x,v1->y,v2->x,v2->y,v->x,v->y);
    if ((area1 + area2)*0.90 <= area0){
      return false;
    }
    v1 = v0;
    v0 = v2;
    for (auto &e_ : c2->Edges){
      if ((e_->vert1 == v2 && e_->vert2 ==v ) ||
	  (e_->vert2 == v2 && e_->vert1 ==v )){
	e = e_;
	break;
      }
    }
    for (auto &c_:e->Cells){
      if (c_ != c2){
	c1 = c2;
	c2 = c_;
	break;
      }
    }
    for (auto &v_:c2->Vertices){
      if (v_ != v && v_ != v0){
	v2 = v_;
	break;
      }
    }
  }
  return true;
}
