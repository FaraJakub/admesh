#include "mesher.h"

Mesher::Mesher(py::array_t<double> Coordinates, py::array_t<int> Cells_arr, py::array_t<int> Edge_arr){
  py::buffer_info cells_buffer = Cells_arr.request();
  py::buffer_info coordinates_buffer = Coordinates.request();
  py::buffer_info edges_buffer = Edge_arr.request();
  
  
  if (coordinates_buffer.ndim != 2)
    throw std::runtime_error("Number of dimensions must be 2!");
  double* Coor_1d = (double*) coordinates_buffer.ptr;
  int Y = coordinates_buffer.shape[0];
  for (int indy = 0;indy<2*Y;indy+=2){
    vertex *v = new vertex;
    v->x = Coor_1d[indy];
    v->y = Coor_1d[1 + indy];
    v->index = indy/2;
    Vertices.push_back(v);
  }
  int* Cells_1d = (int*)  cells_buffer.ptr;
  Y = cells_buffer.shape[0];
  int vert1,vert2,vert3;
  for (int indy = 0;indy<3*Y;indy+=3){
    cell *c = new cell;
    vert1 = Cells_1d[indy];
    vert2 = Cells_1d[1 + indy];
    vert3 = Cells_1d[2 + indy];
    c->Vertices.push_back(Vertices[vert1]);
    c->Vertices.push_back(Vertices[vert2]);
    c->Vertices.push_back(Vertices[vert3]);
    c->d = d(c);
    Cells.push_back(c);
    (Vertices[vert1]->Cells).insert(Vertices[vert1]->Cells.end(),c);
    (Vertices[vert2]->Cells).insert(Vertices[vert2]->Cells.end(),c);
    (Vertices[vert3]->Cells).insert(Vertices[vert3]->Cells.end(),c);
  }
  int* Edges_1d = (int*) edges_buffer.ptr;
  Y = edges_buffer.shape[0];
  for (int indy = 0; indy<2*Y; indy +=2){
    edge *e = new edge;
    Edges.push_back(e);
    e->vert1 = Vertices[Edges_1d[indy]];
    e->vert2 = Vertices[Edges_1d[indy +1]];
    (Vertices[Edges_1d[indy]]->Edges).push_back(e);
    (Vertices[Edges_1d[indy+1]]->Edges).push_back(e);
  }
  //connect cells and edges
  vertex *v0,*v1,*v2;
  for (auto & c : Cells){
    v0 = c->Vertices[0];
    v1 = c->Vertices[1];
    v2 = c->Vertices[2];
    for (auto & e: v0->Edges){
      if ((e->vert1 == v1) || (e->vert2 == v1)){
	c->Edges.push_back(e);
	e->Cells.push_back(c);
	break;
      }
    }

    for (auto & e: v0->Edges){
      if ((e->vert1 == v2) || (e->vert2 == v2)){
	c->Edges.push_back(e);
	e->Cells.push_back(c);
	break;
      }
    }

    for (auto & e: v1->Edges){
      if ((e->vert1 == v2) || (e->vert2 == v2)){
	c->Edges.push_back(e);
	e->Cells.push_back(c);
	break;
      }
    }
  }
  set_outer_bndry();
}

int Mesher::num_of_vertices(){
  return Vertices.size();
}

void Mesher::move_mesh(py::array_t<double> Coordinates){
  py::buffer_info coordinates_buffer = Coordinates.request();  
  if (coordinates_buffer.ndim != 2)
    throw std::runtime_error("Number of dimensions must be 2!");
  
  double* Coor_1d = (double*) coordinates_buffer.ptr;
  int Y = coordinates_buffer.shape[0];
  for (int indy = 0;indy<Y;indy++){
    Vertices[indy]->x += Coor_1d[2*indy];
    Vertices[indy]->y += Coor_1d[2*indy+1];
    for(auto & c : Vertices[indy]->Cells ){
      c->d = d(c);
    }
  }
}


void Mesher::refine_cell(cell* c){
  c->d = -1; //cell will be deleted
  double new_vx = (c->Vertices[0]->x +c->Vertices[1]->x + c->Vertices[2]->x)/3;
  double new_vy = (c->Vertices[0]->y +c->Vertices[1]->y + c->Vertices[2]->y)/3;
  vertex* v = new vertex;
  v->x = new_vx;
  v->y = new_vy;
  //initializing of the new cells
  cell*c1 = new cell;
  cell*c2 = new cell;
  cell*c3 = new cell;
  c1->Vertices = {c->Vertices[0],c->Vertices[1],v};
  c2->Vertices = {c->Vertices[1],c->Vertices[2],v};
  c3->Vertices = {c->Vertices[0],c->Vertices[2],v};
  for(auto & v : c1->Vertices){
    v->Cells.push_back(c1);
  }
  for(auto & v : c2->Vertices){
    v->Cells.push_back(c2);
  }
  for(auto & v : c3->Vertices){
    v->Cells.push_back(c3);
  }
  Vertices.push_back(v);
  Cells.push_back(c1);
  Cells.push_back(c2);
  Cells.push_back(c3);
}

//,const bool remove_vert,const bool add_vert ,const bool move
void Mesher::repair_mesh(const double min_r,const double max_r){
  double cos_alpha;
  double cos_beta;
  double cos_gamma;
  double x1,x2,x3,y1,y2,y3,ax,ay,bx,by;
  //cout<<"started"<<endl;
  //for(auto & c:Cells){
  for(unsigned long int i=0;i<Cells.size();i++){
    cell* c = Cells[i];
    //cout<<" i = "<<i<<endl;
    if( c->d !=-1 && (r(c)<min_r || area(c)<7*min_r*min_r)){
      //cout<<"delete r= "<<r(c)<<" max_r = "<<max_r<<endl;
      x1 = c->Vertices[0]->x;
      y1 = c->Vertices[0]->y;
      x2 = c->Vertices[1]->x;
      y2 = c->Vertices[1]->y;
      x3 = c->Vertices[2]->x;
      y3 = c->Vertices[2]->y;
      if ((c->Vertices[0]->bndry==false)){
	ax = x2-x1;
	bx = x3-x1;
	ay = y2-y1;
	by = y3-y1;
	cos_alpha= (ax*bx + ay*by)/(sqrt(ax*ax+ay*ay)*sqrt(bx*bx+by*by));
      }else{
	cos_alpha = 1.;
      }
      if(c->Vertices[1]->bndry==false){
	ax = x1-x2;
	bx = x3-x2;
	ay = y1-y2;
	by = y3-y2;
	cos_beta= (ax*bx + ay*by)/(sqrt(ax*ax+ay*ay)*sqrt(bx*bx+by*by));
      }else{
	cos_beta = 1.;
      }
      if(c->Vertices[2]->bndry==false){
	ax = x1-x3;
	bx = x2-x3;
	ay = y1-y3;
	by = y2-y3;
	cos_gamma= (ax*bx + ay*by)/(sqrt(ax*ax+ay*ay)*sqrt(bx*bx+by*by));
      }else{
	cos_gamma = 1.;
      }
      if (min(min(cos_alpha,cos_beta),cos_gamma)<1){
	if (cos_alpha<cos_beta){
	  if (cos_alpha<cos_gamma){
	    remove_vertex(c->Vertices[0]);
	  }else{
	    remove_vertex(c->Vertices[2]);
	  }
	}else{
	  if(cos_beta<cos_gamma){
	    remove_vertex(c->Vertices[1]);
	  }else{
	    remove_vertex(c->Vertices[2]);
	  }
	}
      }
    }
  }
  unsigned long int size = Cells.size();
  for(unsigned long int i=0; i<size;i++){
    cell* c = Cells[i];
    if(c->d !=-1 && (area(c)>50*max_r*max_r || max_edge(c) >max_r )&& c->Vertices.size() ==3){
      cout<<"area = "<<area(c)<<" limit = "<<7*max_r*max_r<<endl;
      //add_vert_edge(c);
    }
  }
  //if(move ==true){
  cout<<"wrong cells deleted! \n";
  cout<<"start moving vertices \n";
  move_vertices();
  cout<<"moving finished! \n";
  //}
}

void Mesher::print_wrong_entities(string test_name){
  cout<<"TEST: "<<test_name<<endl;
  int IEC=0,BEC=0,CE=0,VE=0,CV=0,VC=0,EV=0,MEV=0;
  string iec,bec,ce,ve,cv,vc,ev,mev;
  iec = "Wrong inner edge number of cells = ";
  bec = "Wrong bndry edge number of cells = ";
  ce = "Wrong cell in edge";
  ve = "Wrong vertex in edge";
  vc = "wrong vertex in cell";
  ev = "Wrong edge in vertex";
  cv = "Wrong vertex in cell";
  mev = "Missing edge in vertex";
    vector<string> errors= {iec,bec,ce,ve,cv,vc,ev,mev};
  for (auto &e : Edges){
    if (e->usable ==true){
      if ( e->Cells.size() != 2 && e->bndry ==false){
	IEC +=1;
	/*
	cout<<e->vert1->x<<" "<<e->vert1->y<<" "<<e->vert2->x<<" "<<e->vert2->y<<endl;
	cout<<e->Cells.size()<<endl;
	for (auto &c:e->Cells){
	  cout<<c->d<<endl;
	}
	*/
      }
      if ( e->Cells.size() != 1 && e->bndry ==true){
	if (e->Cells[0]->mark == e->Cells[1]->mark){
	  BEC+=1;
	}
      }
      for (auto c:e->Cells){
	if (c->d ==-1){
	  CE+=1;
	  //cout<<"num of cells is: "<<e->Cells.size()<<endl;
	  //cout<<"bndry = "<<e->bndry<<endl;
	}
      }
      if (e->vert1->index== -1 || e->vert2->index== -1){
	VE +=1;
	//cout<<"("<<e->vert1->x<<","<<e->vert1->y<<") "<<" ("<<e->vert2->x<<","<<e->vert2->y<<") "<<endl;
      }
    }
  }
  for (auto &c :Cells){
    if (c->d != -1){
      if (c->d != -1 && c->Edges.size() != 3 && c->Vertices.size() != 3){
	//cout<<"Wrong cell number of Vertices :"<<c->Vertices.size()<<" number of Edges: "<< c->Edges.size()<<endl;
      }
      for (auto &v :c->Vertices){
	if (v->index ==-1){
	  VC +=1;
	}
      }
    }
  }
  vertex *v1,*v2,*v3;
  vector<vertex*> vertices;
  int num_edges;
  for (auto &c : Cells){
    if (c->Vertices.size()==3 && c->d != -1){
      v1 =c->Vertices[0];
      v2 =c->Vertices[1];
      v3 =c->Vertices[2];
      num_edges = 0;
      for (auto e: v1->Edges){
	if (e->vert1 == v2 || e->vert2 == v2){num_edges +=1;}
	if (e->vert1 == v3 || e->vert2 == v3){num_edges +=1;}
      }
      if (num_edges !=2){
	/*
	cout<<v1->x<<" "<<v2->y<<endl;
	for(auto e: c->Edges){
	  cout<<" ("<<e->vert1->x<<", "<<e->vert1->y<<")  ("<<e->vert2->x<<", "<<e->vert2->y<<") \n";
	  }*/
	MEV +=1;}
      num_edges = 0;
      for (auto e: v2->Edges){
	if (e->vert1 == v1 || e->vert2 == v1){num_edges +=1;}
	if (e->vert1 == v3 || e->vert2 == v3){num_edges +=1;}
      }
      if (num_edges !=2){
	MEV +=1;}
      num_edges = 0;
      for (auto e: v3->Edges){
	if (e->vert1 == v2 || e->vert2 == v2){num_edges +=1;}
	if (e->vert1 == v1 || e->vert2 == v1){num_edges +=1;}
      }
      if (num_edges !=2){
	MEV +=1;}
    }
  }
  cell *c1,*c2;
  for (auto &v: Vertices){
    if (v->index != -1){
      for (auto &e: v->Edges){
	if (e->usable == false){
	  EV +=1;
	}
      }
      for (auto &c : v->Cells){
	if (c->d ==-1){
	  CV +=1;
	}
      }
      /*
      for (int i = 0;i <= v->Cells.size();i++){
	for (int j = i +1;j <= v->Cells.size();j++){
	  c1 = v->Cells[i];
	  c2 = v->Cells[j];
	  for (auto &v1:c1->Vertices){
	    for (auto &v2: c2->Vertices){
	      
	    }
	  }
	}
      }
      */
    }
  }
  vector<int> indicators= {IEC,BEC,CE,VE,CV,VC,EV,MEV};
  for (long unsigned int i = 0;i<indicators.size();i++){
    if (indicators[i]>0){
      cout<<errors[i]<<". #"<<indicators[i]<<endl;
    }
  }
  cout<<"end of TEST: "<<test_name<<endl;

}

