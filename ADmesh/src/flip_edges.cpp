#include "mesher.h"

void Mesher::flip_edge_i(int index){
  edge *e = Edges[index];
  if (e->usable==true){
    cout<<"flipping"<<endl;
    flip_edge(e);
  }else{cout<<"Warning you tried to flip unusable edge"<<endl;}
}

void Mesher::flip_edge(edge* e){
  if (e->Cells.size()==2){
    edge* e_new = new edge;
    vertex *v1,*v2,*v3,*v4;
    edge *e13,*e23,*e24,*e14;
    cell *c1 = e->Cells[0];
    cell *c2 = e->Cells[1];
    c1->d = -1;
    c2->d = -1;
    e->usable=false;
    v1 = e->vert1;
    v2 = e->vert2;
    for (auto &v:c1->Vertices){
      if (v !=v1 && v != v2){
	v3 = v;
	break;
      }
    }
    for (auto &v:c2->Vertices){
      if (v !=v1 && v != v2){
	v4 = v;
	break;
      }
    }
    e_new->vert1 = v3;
    e_new->vert2 = v4;
    v3->Edges.push_back(e_new);
    v4->Edges.push_back(e_new);
    //find remaining vertices
    for (auto &e_: c1->Edges){
      if (e_ != e && (e_->vert1 ==v1 || e_->vert2 ==v1 )){
	e13 = e_;
      }else if (e_ != e && (e_->vert1 ==v2 || e_->vert2 ==v2 )){
	e23 = e_;
      }      
    }
    for (auto &e_: c2->Edges){
      if (e_ != e && (e_->vert1 ==v1 || e_->vert2 ==v1 )){
	e14 = e_;
      }else if (e_ != e && (e_->vert1 ==v2 || e_->vert2 ==v2 )){
	e24 = e_;
      }      
    }
    //create new cells
    if (v1==v2){
      cout<<"--------------------------------------------------------------------------------------------------------";
    }
    create_cell(e13,e14,e_new,v1,v3,v4,c1->mark, 3);
    create_cell(e23,e24,e_new,v2,v3,v4,c1->mark, 3);
    
    //delete old dependencies
    vector<vertex*> verts= {v1,v2,v3,v4};
    for (auto &v: verts){
      for (auto cl = v->Cells.begin(); cl != v->Cells.end(); cl++) {
	if ((*cl)->d == -1){
	  v->Cells.erase(cl--);
	}
      }    
    }
    vector<edge*> edgs= {e13,e23,e24,e14};
    for (auto &e_: edgs){
      for (auto cl = e_->Cells.begin(); cl != e_->Cells.end(); cl++) {
	if ((*cl)->d == -1){
	  e_->Cells.erase(cl--);
	}
      }    
    }

    for (auto ed = v1->Edges.begin(); ed != v1->Edges.end(); ed++) {
      if ((*ed) == e){
	v1->Edges.erase(ed--);
      }
    }
    for (auto ed = v2->Edges.begin(); ed != v2->Edges.end(); ed++) {
      if ((*ed) == e){
	v2->Edges.erase(ed--);
      }
    }
    Edges.push_back(e_new);
 
    /*
    if (e_new->Cells.size() != 2 || e13->Cells.size() != 2 || e23->Cells.size() != 2 || e24->Cells.size() != 2 ||e14->Cells.size() != 2){
      cout<<"#cells in flipped edge: "<<e_new->Cells.size()<<endl;
      for (auto &c:e_new->Cells){
	cout<<"   #vertices in cell: "<<c->Vertices.size()<<endl;
	for (auto &e_:c->Edges){
	  cout<<"         #Cells in edge:"<<e_->Cells.size()<<" bndry = "<<e_->bndry<<endl;
	}
      }
      }*/
    //Cells.remove(Cells.begin(),Cells.end(),c1);
    //Cells.erase(&c2);
    //Edges.erase(&e);
    //delete c1;
    //delete c2;
    //delete e;
  }else{cout<<"This edge can not be flipped!! Number of cells is : "<<e->Cells.size()<<endl;}
}

bool Mesher::is_valid_edge(edge *e){
  if (e->Cells.size() ==2 && e->bndry == false){
    for (auto &c : e->Cells){
      if (c->Vertices.size() != 3){
	cout<<"wrong number of vertices in cell \n";
	return false;
      }
      for (auto &v :c->Vertices){
	if (v->bndry == false){
	  if (v->Cells.size() != v->Edges.size()){
	    cout<<"number of edges and number of Cells in vertex does not agree \n";
	    return false;
	  }
	}else{
	  if (v->Cells.size() != v->Edges.size() -1 ){
	    cout<<"number of edges and number of Cells in boundary vertex does not agree \n";
	    return false;
	  }	  
	}
      }
    }
  }
  return true;
}
