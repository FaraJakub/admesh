#include "mesher.h"

void Mesher::create_cell(edge *e1,edge *e2,edge *e3,vertex *v1,vertex *v2,vertex *v3,int mark,int d_){
  cell* c = new cell;
  c->mark = mark;
  c->Vertices.push_back(v1);
  c->Vertices.push_back(v2);
  c->Vertices.push_back(v3);
  c->Edges.push_back(e1);
  c->Edges.push_back(e2);
  c->Edges.push_back(e3);
  e1->Cells.push_back(c);
  e2->Cells.push_back(c);
  e3->Cells.push_back(c);
  v1->Cells.push_back(c);
  v2->Cells.push_back(c);
  v3->Cells.push_back(c);
  Cells.push_back(c);
  c->d = d_;
}

void Mesher::add_vert_edge_i(int index){
  edge* e = Edges[index];
  add_vert_edge(e);
}


void Mesher::add_vert_edge(edge *e){
  vertex *v1 = e->vert1;
  vertex *v2 = e->vert2;
  vertex* v_new = new vertex;
  Vertices.push_back(v_new);
  v_new->x =0.5*( v1->x + v2->x);
  v_new->y =0.5*( v1->y + v2->y);
  v_new->remesh_param = 0.5*(v1->remesh_param + v2->remesh_param);
  edge* e1 = new edge;
  edge* e2 = new edge;
  edge* e3 = new edge;

  e1->vert1 = v1;
  e1->vert2 = v_new;
  e2->vert1 = v_new;
  e2->vert2 = v2;

  e->usable = false;
  if(e->bndry == true){
    e1->bndry =true;
    e2->bndry =true;
    v_new->bndry = true;
  }
  if(e->interface == true){
    e1->interface =true;
    e2->interface =true;
    v_new->interface = true;
  }
     
  for (auto c: e->Cells){
    c->d = -1;
  }
  //inner edge
  if ( e->Cells.size() == 2){
    edge* e4 = new edge;
    //cout<<"inner"<<endl;
    cell *c1 = e->Cells[0];
    cell *c2 = e->Cells[1];
    
    vertex *v3,*v4;
    edge *e13,*e23,*e24,*e14;
    for (auto &v: c1->Vertices){
      if (v != v1 && v!= v2){
	v3 = v;
	break;
      }
    }
    for (auto &v: c2->Vertices){
      if (v != v1 && v!= v2){
	v4 = v;
	break;
      }
    } 
    for (auto &e_: c1->Edges){
      if (e_ != e && (e_->vert1 ==v1 || e_->vert2 ==v1 )){
	e13 = e_;
      }else if (e_ != e && (e_->vert1 ==v2 || e_->vert2 ==v2 )){
	e23 = e_;
      }      
    }
    for (auto &e_: c2->Edges){
      if (e_ != e && (e_->vert1 ==v1 || e_->vert2 ==v1 )){
	e14 = e_;
      }else if (e_ != e && (e_->vert1 ==v2 || e_->vert2 ==v2 )){
	e24 = e_;
      }      
    }
    e3->vert1 = v3;
    e3->vert2 = v_new;
    e4->vert1 = v4;
    e4->vert2 = v_new;
    create_cell(e1,e13,e3,v1,v3,v_new,c1->mark,2);
    create_cell(e2,e23,e3,v2,v3,v_new,c1->mark,2);
    create_cell(e1,e14,e4,v1,v4,v_new,c2->mark,2);
    create_cell(e2,e24,e4,v2,v4,v_new,c2->mark,2);
    Edges.push_back(e1);
    Edges.push_back(e2);
    Edges.push_back(e3);
    Edges.push_back(e4);
    v1->Edges.push_back(e1);
    v2->Edges.push_back(e2);
    v3->Edges.push_back(e3);
    v4->Edges.push_back(e4);
    v_new->Edges.push_back(e1);
    v_new->Edges.push_back(e2);
    v_new->Edges.push_back(e3);
    v_new->Edges.push_back(e4);

    vector<vertex*> vertices = {v1,v2,v3,v4};
    for (auto &v : vertices ){
      for (auto cl = v->Cells.begin(); cl != v->Cells.end(); cl++) {
	if ((*cl)->d ==-1){
	  v->Cells.erase(cl--);
	}
      }
    }
    for (auto ed = v1->Edges.begin(); ed != v1->Edges.end(); ed++) {
      if ((*ed)->usable == false){
	v1->Edges.erase(ed--);
      }
    }
    for (auto ed = v2->Edges.begin(); ed != v2->Edges.end(); ed++) {
      if ((*ed)->usable == false){
	v2->Edges.erase(ed--);
      }
    }
    vector<edge*> edges = {e13,e23,e24,e14};
    for (auto ed:edges){
      for (auto c =  ed->Cells.begin();c !=ed->Cells.end(); c++){
	if ((*c)->d == -1){
	  ed->Cells.erase(c--);
	}
      }
    }
    //delete c1;
    //delete c2;
    //delete e;
    //boundary edge
  }else if ( e->Cells.size() == 1 && e-> bndry ==true){
    //cout<<"bndry"<<endl;
    cell *c1 = e->Cells[0];
    vertex *v3;
    edge *e13,*e23;
    e1->bndry = true;
    e2->bndry = true;
    for (auto &v: c1->Vertices){
      if (v != v1 && v!= v2){
	v3 = v;
	break;
      }
    }
    for (auto &e_: c1->Edges){
      if (e_ != e && (e_->vert1 ==v1 || e_->vert2 ==v1 )){
	e13 = e_;
      }else if (e_ != e && (e_->vert1 ==v2 || e_->vert2 ==v2 )){
	e23 = e_;
      }      
    }
    e3->vert1 = v3;
    e3->vert2 = v_new;
    create_cell(e1,e13,e3,v1,v3,v_new,c1->mark,  4);
    create_cell(e2,e23,e3,v2,v3,v_new,c1->mark, 4);
    v1->Edges.push_back(e1);
    v2->Edges.push_back(e2);
    v3->Edges.push_back(e3);
    v_new->Edges.push_back(e1);
    v_new->Edges.push_back(e2);
    v_new->Edges.push_back(e3);
    Edges.push_back(e1);
    Edges.push_back(e2);
    Edges.push_back(e3);

    
    vector<vertex*> vertices = {v1,v2,v3};
    for (auto &v : vertices ){
      for (auto cl = v->Cells.begin(); cl != v->Cells.end(); cl++) {
	if ((*cl)->d ==-1){
	  v->Cells.erase(cl--);
	}
      }
    }


    for (auto ed = v1->Edges.begin(); ed != v1->Edges.end(); ed++) {
      if ((*ed)->usable == false){
	v1->Edges.erase(ed--);
      }
    }
    for (auto ed = v2->Edges.begin(); ed != v2->Edges.end(); ed++) {
      if ((*ed)->usable == false){
	v2->Edges.erase(ed--);
      }
    }
    vector<edge*> edges = {e13,e23};
    for (auto ed:edges){
      for (auto c =  ed->Cells.begin();c !=ed->Cells.end(); c++){
	if ((*c)->d == -1){
	  ed->Cells.erase(c--);
	}
      }
    }
	
    //delete c1;
    //delete e;

  }else{
    cout<<"something went wrong with edge!!!!"<<endl;
    //cout<<"bndry = "<<e->bndry<<" #cells = "<<e->Cells.size()<<" coordinates = ("<<e->vert1->x<<","<<e->vert1->y<<"),("<<e->vert2->x<<","<<e->vert2->y<<")\n";
    }
}
