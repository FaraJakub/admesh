#include "mesher.h"

py::array_t<double> Mesher::getCoordinates(){
  int len = Vertices.size();
  py::array_t<double> result = py::array_t<double>(2*len);
  py::buffer_info buf = result.request();
  double * ptr = (double *) buf.ptr;
  int i = 0;
  int j = 0;
  for (auto v = Vertices.begin(); v != Vertices.end(); v++) {
    if ((*v)->index !=-1){
      ptr[2*i] = (*v)->x;
      ptr[2*i+1] = (*v)->y;
      //cout<<"("<<(*v)->x<<" ,"<<(*v)->y<<") \n";
      (*v)->index =i;
      i+=1;
    }else{
      j+=1;
      delete (*v);
      Vertices.erase(v--);
    }
  }
  result.resize({i,2});
  return result;
}

py::array_t<int> Mesher::getCells(){
  int len = Cells.size();
  py::array_t<int> result = py::array_t<int>(3*len);
  py::buffer_info buf = result.request();
  int * ptr = (int *) buf.ptr;
  int i = 0;
  for (auto c = Cells.begin(); c != Cells.end(); c++) {
    //cout<<"size = "<<cell->Vertices.size()<<"\n";
    if ((*c)->Vertices.size()==3 && (*c)->d != -1){
      ptr[3*i] = (*c)->Vertices[0]->index;
      ptr[3*i+1] = (*c)->Vertices[1]->index;
      ptr[3*i+2] = (*c)->Vertices[2]->index;
      //cout<<(*c)->d<<" \n ";
      //cout<<(*c)->Vertices[0]->index<<" "<<(*c)->Vertices[1]->index<<" "<<(*c)->Vertices[2]->index<<"\n";
      i+=1;
    }else{
      delete (*c);
      Cells.erase(c--);
      //cout<<"erased \n";
    }
    //else{cout<<"error cell \n";}
  }
  result.resize({i,3});
  return result;
}

py::array_t<int> Mesher::getMarks(){
  int len = Cells.size();
  py::array_t<int> result = py::array_t<int>(len);
  py::buffer_info buf = result.request();
  int * ptr = (int *) buf.ptr;
  int i = 0;
  for (auto c = Cells.begin(); c != Cells.end(); c++){
    if ((*c)->Vertices.size()==3 && (*c)->d != -1.0){
      ptr[i] = (*c)->mark;
      i+=1;
    }else{
      delete (*c);
      Cells.erase(c--);
    }
  }
  return result;

}

py::array_t<double> Mesher::getBoundaryCoordinates(){
  vector<vertex*> vec; 
  for (auto &v : Vertices){
    if(v->bndry == true){
      vec.push_back(v);
    }
  }
  int len = vec.size();
  py::array_t<double> result = py::array_t<double>(2*len);
  py::buffer_info buf = result.request();
  double * ptr = (double *) buf.ptr;
  int i = 0;
  for (auto v = vec.begin(); v != vec.end(); v++) {
    if ((*v)->index !=-1){
      ptr[2*i] = (*v)->x;
      ptr[2*i+1] = (*v)->y;
      i+=1;
    }
  }
  result.resize({i,2});
  return result;
}

double Mesher::get_rmin(){
  double rmin = 1000.0;
  double r_;
  for(auto & c:Cells){
    r_ = r(c);
    if(r_<rmin){rmin = r_;}
  }
  return rmin;
}

double Mesher::get_rmax(){
  double rmax = 0.0;
  double r_;
  for(auto & c:Cells){
    r_ = r(c);
    if(r_>rmax){rmax = r_;}
  }
  return rmax;
}
