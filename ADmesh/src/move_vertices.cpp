#include "mesher.h"

void Mesher::move_vertices(){
  int n;
  double X,Y;
  for(auto & v : Vertices ){
    if(v->bndry ==false && v->interface == false && v->index != -1 && is_convex(v) == true){
      X = 0;
      Y = 0;
      n = 0;
      for(auto & c : v->Cells){
	if(c->d != -1 && c->Vertices.size()==3){
	  n +=1;
	  for(auto & ve : c->Vertices){
	    if(ve->index != -1){
	      if( ve!= v){
		X += ve->x;
		Y += ve->y;
	      }
	    }
	  }
	}
      }
      v->x = X/(2*n);
      v->y = Y/(2*n);
    }
  }
}
