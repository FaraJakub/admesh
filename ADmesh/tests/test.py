import admesh as m
import numpy as np

vertices = np.array([[0.0,0.0],[0.0,1.0],[1.0,0.0],[1.0,1.0]])
cells = np.array([[0,1,3],[0,2,3]],dtype = 'int32')
edges = np.array([[0,1],[0,2],[0,3],[1,3],[2,3]])

mesher = m.Mesher(vertices,cells,edges)
